# Step 1: Use an official Node.js runtime as a parent image
FROM node:20-alpine

# Step 2: Set the working directory in the container
WORKDIR /usr/src/app

# Step 3: Install NestJS CLI globally within the container
RUN npm install -g @nestjs/cli

# Step 4: Copy package.json files and install dependencies
COPY package*.json ./
RUN npm install

# Step 5: Copy the application source files into the container
COPY . .

# Step 6: Build the application for production
RUN npm run build

# Step 7: Map the port the app runs on
EXPOSE 3000

# Step 8: Define the command to run the app
CMD ["npm", "run", "start:prod"]
