import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { TelegrafModule } from 'nestjs-telegraf';

import { KarmabotModule } from './karmabot/karmabot.module';
import { ChiguirebotModule } from './chiguirebot/chiguire.module';
import { SupabaseModule } from './supabase/supabase.module';
import { CronminderModule } from './cronminder/cronminder.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    KarmabotModule,
    ChiguirebotModule,
    SupabaseModule,
    CronminderModule,
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true,
      envFilePath: `.${process.env.NODE_ENV}.env`,
    }),
    TelegrafModule.forRootAsync({
      imports: [ConfigModule],
      botName: 'Doctor Karma',
      useFactory: async (configService: ConfigService) => ({
        token: configService.get<string>('KARMA_BOT'),
        include: [KarmabotModule],
      }),
      inject: [ConfigService],
    }),
    TelegrafModule.forRootAsync({
      imports: [ConfigModule],
      botName: 'Chiguirebot',
      useFactory: async (configService: ConfigService) => ({
        token: configService.get<string>('CHIGUIRE_BOT'),
        include: [ChiguirebotModule],
      }),
      inject: [ConfigService],
    }),
    TelegrafModule.forRootAsync({
      imports: [ConfigModule],
      botName: 'Cronminder',
      useFactory: async (configService: ConfigService) => ({
        token: configService.get<string>('CRONMINDER_BOT'),
        include: [CronminderModule],
      }),
      inject: [ConfigService],
    }),
  ],
})
export class AppModule {}
