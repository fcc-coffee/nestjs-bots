import { Module, Provider } from '@nestjs/common';
import { ChiguirebotService } from './services/chiguirebot.service';
import { ChiguirebotController } from './commands/chiguirebot.controller';
import { HandleHTTP } from 'src/common/common.interface';

@Module({
  providers: [
    ChiguirebotService,
    ChiguirebotController,
    HandleHTTP as Provider,
  ],
})
export class ChiguirebotModule {}
