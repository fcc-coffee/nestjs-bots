import { Test, TestingModule } from '@nestjs/testing';
import { ChiguirebotController } from './chiguirebot.controller';

describe('ChiguirebotController', () => {
  let controller: ChiguirebotController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChiguirebotController],
    }).compile();

    controller = module.get<ChiguirebotController>(ChiguirebotController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
