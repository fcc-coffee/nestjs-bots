import { Command, Ctx, Help, On, Start, Update } from 'nestjs-telegraf';
import { TelegrafContext } from 'src/telegraf/telegraf.interface';
import { ChiguirebotService } from '../services/chiguirebot.service';

@Update()
export class ChiguirebotController {
  constructor(private chiguireServices: ChiguirebotService) {}
  @Start()
  async start(@Ctx() ctx: TelegrafContext) {
    await ctx.reply(
      '¿Deseas obtener las cotizaciones más importantes en Venezuela?.\n\nIndicame que cotización quieres observar por medio de mis comandos, para más información enviame /help. ',
    );
  }

  @Help()
  async help(@Ctx() ctx: TelegrafContext) {
    await ctx.reply(
      'Tienes la lista de comandos\n\n/yadio: Promedio de Yadio.io\n/bcv: Promedio del Banco Central de Venzuela',
    );
  }

  @Command('bcv')
  async bcvCommand(ctx: TelegrafContext) {
    const { name, price, lastUpdate } = await this.chiguireServices.getBCV();
    await ctx.reply(
      `Para ${name} el precio es ${price} y la ultima fecha de actualizacion ${lastUpdate}`,
    );
  }

  @Command('yadio')
  async yadioCommand(ctx: TelegrafContext) {
    const { name, price, lastUpdate } = await this.chiguireServices.getYADIO();
    await ctx.reply(
      `Para ${name} el precio es ${price} y la ultima fecha de actualizacion ${lastUpdate}`,
    );
  }
}
