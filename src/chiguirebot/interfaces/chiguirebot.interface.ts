export interface ISourceDetail {
  last_retrieved: string;
  name: string;
  quote: string;
}

interface ISources {
  BCV: ISourceDetail;
  Yadio: ISourceDetail;
}

export interface IPayload {
  name: string;
  pair: string;
  sources: ISources;
}

export interface IChiguirebot {
  baseUri: string;
  routes: Record<string, string>;
}

export interface IFormatResponse {
  name: string;
  price: GLfloat;
  lastUpdate: string;
}
