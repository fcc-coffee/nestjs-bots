import { Test, TestingModule } from '@nestjs/testing';
import { ChiguirebotService } from './chiguirebot.service';

describe('ChiguirebotService', () => {
  let service: ChiguirebotService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChiguirebotService],
    }).compile();

    service = module.get<ChiguirebotService>(ChiguirebotService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
