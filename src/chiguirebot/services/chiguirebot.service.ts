import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { HandleHTTP } from 'src/common/common.interface';
import { DataValidate } from '../utils/validate.data';
import {
  IPayload,
  IChiguirebot,
  IFormatResponse,
} from '../interfaces/chiguirebot.interface';

@Injectable()
export class ChiguirebotService {
  protected baseUri: IChiguirebot['baseUri'];
  protected routes: IChiguirebot['routes'];
  private dataValidate: DataValidate;

  constructor(
    private readonly configService: ConfigService,
    private readonly httpManager: HandleHTTP,
  ) {
    this.baseUri = this.configService.get<string>('BASEURI');
    const routesString = this.configService.get<string>('ENDPOINTS');
    this.routes = JSON.parse(routesString);
    this.dataValidate = new DataValidate();
  }

  public async getBCV(): Promise<IFormatResponse> {
    try {
      const response = await this.httpManager.get<IPayload, typeof this.routes>(
        this.baseUri,
        this.routes.BCV,
      );
      const data = { sources: { BCV: response.sources.BCV } };
      console.log(`Data del objeto ${JSON.stringify(data.sources.BCV)}`);

      try {
        const formattedResponse = DataValidate.formatData(
          data,
          'BCV',
          'America/Caracas',
          'YYYY-MM-DD HH:mm:ss',
        );
        return formattedResponse;
      } catch (error) {
        console.error('Error formatting data:', error);
      }
      console.log('Routes:', this.routes);
      console.log('Response:', response);
      console.log('Response Sources:', response.sources);
    } catch (error) {
      return error;
    }
  }

  public async getYADIO(): Promise<IFormatResponse> {
    try {
      const response = await this.httpManager.get<IPayload, typeof this.routes>(
        this.baseUri,
        this.routes.YADIO,
      );
      const data = { sources: { YADIO: response.sources.Yadio } };
      console.log(`Data del objeto ${JSON.stringify(data.sources.YADIO)}`);

      try {
        const formattedResponse = DataValidate.formatData(
          data,
          'YADIO',
          'America/Caracas',
          'YYYY-MM-DD HH:mm:ss',
        );
        return formattedResponse;
      } catch (error) {
        console.error('Error formatting data:', error);
      }
      console.log('Routes:', this.routes);
      console.log('Response:', response);
      console.log('Response Sources:', response.sources);
    } catch (error) {
      return error;
    }
  }
}
