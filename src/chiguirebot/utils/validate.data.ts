import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';

import {
  IFormatResponse,
  ISourceDetail,
} from 'src/chiguirebot/interfaces/chiguirebot.interface';

dayjs.extend(utc);
dayjs.extend(timezone);

export class DataValidate {
  static formatData<
    T extends {
      sources: Record<string, ISourceDetail>;
    },
  >(
    data: T,
    sourceKey: string,
    targetTimezone: string,
    format: string,
  ): IFormatResponse | null {
    const source = data.sources[sourceKey];
    if (!source) return null;

    const utcDate = dayjs(source.last_retrieved).utc();
    const formattedDate = utcDate.tz(targetTimezone).format(format);
    const price = Number(parseFloat(source.quote).toFixed(2));

    return {
      name: source.name,
      price: price,
      lastUpdate: formattedDate,
    };
  }
}
