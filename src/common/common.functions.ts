export const getParams = (text: string, separator: string, length: number) => {
  const array = text.replace(/\/[A-Za-z]+[^ ]* ?/, '').split(separator);
  if (array.length < length) {
    return [];
  }

  return array;
};
