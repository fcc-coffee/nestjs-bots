import { SupabaseClient } from '@supabase/supabase-js';

import { Database } from '../supabase/supabase.definitions';

export type Supa = SupabaseClient<Database>;

type Routes<U extends Record<string, string>> = keyof U;
export class HandleHTTP {
  async get<T, R extends Record<string, string>>(
    base: string,
    endpoint: Routes<R>,
  ): Promise<T> {
    try {
      const response = await (
        await fetch(`${base}${endpoint.toString()}`)
      ).json();
      return response as T;
    } catch (error) {
      return error;
    }
  }
}

export const frasesPopulares = [
  'Un nuevo día, un nuevo comienzo. Levántate con esperanza.',
  'Gracias a Dios por otro día de vida. Comienza con gratitud.',
  'La fe mueve montañas. Ten fe en ti mismo y en tus sueños.',
  'Dios siempre está contigo. Siente su presencia en tu vida.',
  'Empieza tu día con una sonrisa. La actitud lo es todo.',
  'La oración es el lenguaje del corazón. Habla con Dios hoy.',
  'La fe es la fuerza que nos mantiene en pie. Sigue adelante con fe.',
  'Cada día es una nueva oportunidad. Aprovecha al máximo.',
  'La vida es corta. Ama profundamente, ríe a menudo y vive sin arrepentimientos.',
  'No te rindas. Incluso el más oscuro de los túneles termina en luz.',
  'La felicidad es una elección. Elige ser feliz hoy.',
  'La paz comienza contigo. Encuéntrala en tu interior.',
  'La vida es un viaje, no un destino. Disfruta del camino.',
  'El éxito no es la clave de la felicidad. La felicidad es la clave del éxito.',
  'No te preocupes por lo que otros piensen de ti. Vive tu vida con integridad.',
  'La gratitud es la actitud más agradecida. Da las gracias hoy.',
  'La bondad es contagiosa. Sé amable con alguien hoy.',
  'La vida es una aventura. Atrévete a vivirla al máximo.',
  'El perdón libera el alma. Perdona hoy y siente la liberación.',
  'La esperanza es lo último que se pierde. Sigue teniendo esperanza',
];

export const frasesMalosDias = [
  'Que cada día de tu vida sea más insoportable que el anterior, ¡buenos días!',
  'Espero que tu día esté lleno de desgracias y malos ratos, ¡buenos días!',
  'Que tengas un día peor que el de ayer, ¡buenos días!',
  'Espero que tu café esté amargo como tus decisiones, ¡buenos días!',
  'Que tus problemas se multipliquen como conejos, ¡buenos días!',
  'Espero que tus ilusiones se desvanezcan como el humo, ¡buenos días!',
  'Que tus esperanzas se hundan como un barco de papel, ¡buenos días!',
  'Que tus sueños se conviertan en pesadillas, ¡buenos días!',
  'Que tus metas se alejen como un espejismo, ¡buenos días!',
  'Que tus éxitos sean tan escasos como la nieve en verano, ¡buenos días!',
  'Espero que tu almohada esté llena de espinas, ¡buenos días!',
  'Que tus pesadillas se hagan realidad, ¡buenos días!',
  'Espero que tu teléfono no deje de sonar con malas noticias, ¡buenos días!',
  'Que tus planes fallen uno tras otro, ¡buenos días!',
  'Espero que tu cama esté llena de chinches, ¡buenos días!',
  'Que tus momentos de felicidad sean cada vez más cortos, ¡buenos días!',
  'Espero que tu vida sea un desastre, ¡buenos días!',
  'Que tus relaciones sean cada vez más tóxicas, ¡buenos días!',
  'Espero que tu jefe te dé más trabajo de lo que puedes soportar, ¡buenos días!',
  'Que tus clientes sean cada vez más exigentes y menos agradecidos, ¡buenos días!',
  'Espero que tu cuenta bancaria esté siempre en rojo, ¡buenos días!',
  'Que tus deudas sean cada vez mayores, ¡buenos días!',
  'Espero que tu salud empeore día a día, ¡buenos días!',
  'Que tus dolores sean cada vez más fuertes, ¡buenos días!',
  'Espero que tu cabeza esté llena de pensamientos negativos, ¡buenos días!',
  'Que tus noches sean cada vez más largas y tus días más cortos, ¡buenos días!',
  'Espero que tu vida sea un infierno, ¡buenos días!',
];

export const frasesMalosDiasDev = [
  'Que tu código esté lleno de errores hoy, ¡buenos días!',
  'Espero que tu servidor se caiga en el peor momento posible, ¡buenos días!',
  'Que tus consultas SQL fallen una tras otra, ¡buenos días!',
  'Espero que tu framework favorito deje de dar soporte, ¡buenos días!',
  'Que tus pruebas unitarias fallen justo antes del deadline, ¡buenos días!',
  'Espero que tu navegador se congele en el momento más inoportuno, ¡buenos días!',
  'Que tus librerías favoritas fallen en producción, ¡buenos días!',
  'Espero que tu código se vuelva cada vez más complejo e inmanejable, ¡buenos días!',
  'Que tus usuarios encuentren todos los bugs posibles, ¡buenos días!',
  'Espero que tu base de datos se corrompa sin posibilidad de recuperación, ¡buenos días!',
  'Que tus API fallen una y otra vez, ¡buenos días!',
  'Espero que tu código se vuelva cada vez más obsoleto, ¡buenos días!',
  'Que tus actualizaciones de sistema fallen justo cuando más las necesites, ¡buenos días!',
  'Espero que tu código se vuelva cada vez más spaghetti, ¡buenos días!',
  'Que tus herramientas de desarrollo fallen en el peor momento posible, ¡buenos días!',
  'Espero que tu código se vuelva cada vez más lento y pesado, ¡buenos días!',
  'Que tus servicios en la nube fallen sin previo aviso, ¡buenos días!',
  'Espero que tu código se vuelva cada vez más difícil de mantener, ¡buenos días!',
  'Que tus actualizaciones de seguridad fallen justo cuando más las necesites, ¡buenos días!',
  'Espero que tu código se vuelva cada vez más propenso a las inyecciones SQL, ¡buenos días!',
  'Que tus certificados SSL expiren sin aviso previo, ¡buenos días!',
  'Espero que tu código se vuelva cada vez más vulnerable a los ataques DDoS, ¡buenos días!',
];
