import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { CronminderService } from './cronminder.service';
import { CronminderUpdates } from './cronminder.updates';
import { SupabaseService } from 'src/supabase/supabase.service';

@Module({
  imports: [ScheduleModule.forRoot()],
  providers: [CronminderService, CronminderUpdates, SupabaseService],
})
export class CronminderModule {}
