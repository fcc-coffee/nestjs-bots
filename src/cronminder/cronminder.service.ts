import { Injectable, OnModuleInit } from '@nestjs/common';
import { SchedulerRegistry } from '@nestjs/schedule';

import { CronJob } from 'cron';
import { InjectBot } from 'nestjs-telegraf';
import { Telegraf } from 'telegraf';

import { TelegrafContext } from '../telegraf/telegraf.interface';
import {
  frasesPopulares,
  frasesMalosDias,
  frasesMalosDiasDev,
  Supa,
} from '../common/common.interface';
import { SupabaseService } from 'src/supabase/supabase.service';

@Injectable()
export class CronminderService implements OnModuleInit {
  supa: Supa;

  constructor(
    @InjectBot('Cronminder') private readonly bot: Telegraf<TelegrafContext>,
    private readonly schedulerRegistry: SchedulerRegistry,
    private readonly supabaseService: SupabaseService,
  ) {
    this.supa = this.supabaseService.getClient();
  }

  async onModuleInit() {
    await this.scheduleReminders();
  }

  addCronJob(name: string, format: string, chatId: string, message?: string) {
    const job = new CronJob(format, () => {
      if (message) {
        this.bot.telegram.sendMessage(chatId, message);
        return;
      }
      const randomMessage = this.getRandomMessage();
      this.bot.telegram.sendMessage(chatId, randomMessage);
    });

    this.schedulerRegistry.addCronJob(name, job);
    job.start();
  }

  private getRandomMessage(): string {
    const listIndex = Math.floor(Math.random() * 3);
    let chosenList;

    switch (listIndex) {
      case 0:
        chosenList = frasesPopulares;
        break;
      case 1:
        chosenList = frasesMalosDias;
        break;
      case 2:
        chosenList = frasesMalosDiasDev;
        break;
    }

    return chosenList[Math.floor(Math.random() * chosenList.length)];
  }

  private async scheduleCronJob(
    name: string,
    chatId: string,
    message: string,
    format: string,
  ) {
    const cronName = `${name}-${chatId}`;

    if (!this.schedulerRegistry.doesExist('cron', cronName)) {
      this.addCronJob(cronName, format, chatId, message);

      let group_id = 0;
      let groupName = '';

      const result = await this.supa
        .from('groups')
        .select('id, name')
        .eq('group_tlg_id', chatId)
        .maybeSingle();

      if (result.error) {
        console.error(result.error);
        return result.error.message;
      }

      if (result.data) {
        const { id, name } = result.data;
        group_id = id;
        groupName = name;
      }

      const { data, error } = await this.supa.from('crons').insert({
        cron: format,
        content: message,
        group_id: group_id,
        cron_name: name,
      });

      if (error) {
        console.error(error);
        return error.message;
      }
      return true;
    } else {
      return false;
    }
  }

  async scheduleDaily(name: string, chatId: string, message: string) {
    const format = '0 7 * * *';
    return this.scheduleCronJob(
      'daily-message-' + name,
      chatId,
      message,
      format,
    );
  }

  async scheduleMonthly(
    name: string,
    chatId: string,
    message: string,
    day: string,
    hour: string,
  ) {
    const format = `0 ${hour} ${day} * *`;
    return this.scheduleCronJob(
      'monthly-message-' + name,
      chatId,
      message,
      format,
    );
  }

  async scheduleWeekly(
    name: string,
    chatId: string,
    message: string,
    day: string,
    hour: string,
  ) {
    const format = `0 ${hour} * * ${day}`;
    return this.scheduleCronJob(
      'weekly-message-' + name,
      chatId,
      message,
      format,
    );
  }

  async scheduleWeekdayMessage(name: string, chatId: string, message: string) {
    const format = '0 7 * * 1-5';
    return this.scheduleCronJob(
      'weekday-message-' + name,
      chatId,
      message,
      format,
    );
  }

  async scheduleDays(
    name: string,
    chatId: string,
    message: string,
    days: string[],
    hour: string,
  ) {
    if (days.length < 1 || days.length > 7) {
      return false;
    }

    if (days.includes('7')) {
      days = days.map((day) => (day === '7' ? '0' : day));
    }
    const format = `0 ${hour} * * ${days.join(',')}`;
    return this.scheduleCronJob(
      'days-message-' + name,
      chatId,
      message,
      format,
    );
  }

  async scheduleYearly(
    name: string,
    chatId: string,
    message: string,
    day: string,
    month: string,
    hour: string,
  ) {
    const format = `0 ${hour} ${day} ${month} *`;
    return this.scheduleCronJob(
      'yearly-message-' + name,
      chatId,
      message,
      format,
    );
  }

  createCronExpression(date: string): string {
    const [year, month, day] = date.split('-').map((val) => parseInt(val));
    if (month > 12 || day > 31) {
      console.log('Invalid date');
      return 'Invalid date';
    }
    return `1 0 ${day} ${month} *`;
  }

  async addBdayReminder(
    firstName: string,
    lastName: string,
    birthdate: string,
    chatId: string,
  ): Promise<string> {
    const cronExpression = this.createCronExpression(birthdate);
    if (cronExpression === 'Invalid date') {
      return 'Invalid date';
    }
    const name = `bday_reminder_${firstName}_${lastName}_${chatId.replace(
      '-',
      '',
    )}-birth=${birthdate}`;

    const {
      data: { id: group_id, name: groupName },
      error: getGroupIdError,
    } = await this.supa
      .from('groups')
      .select('id, name')
      .eq('group_tlg_id', chatId)
      .maybeSingle();

    if (getGroupIdError) {
      console.error(getGroupIdError);
      return getGroupIdError.message;
    }

    const bdayMessage = `Happy birthday ${firstName} ${lastName}`;

    const { data, error } = await this.supa.from('crons').insert({
      cron: cronExpression,
      content: bdayMessage,
      group_id,
      cron_name: name,
    });
    if (error) {
      console.error(error);
      return error.message;
    }

    this.addCronJob(name, cronExpression, chatId, bdayMessage);
    return `Cron created for ${firstName} ${lastName}'s on ${groupName} birthday at ${birthdate}`;
  }

  private async scheduleReminders() {
    const { data, error } = await this.supa
      .from('crons')
      .select('*, groups(group_tlg_id)');

    if (error) {
      console.error('Error fetching reminders:', error);
      return;
    }

    data.forEach((reminder) => {
      if (
        reminder.cron_name.includes('bday') &&
        parseInt(reminder.cron.split(' ')[3]) > 12
      ) {
        return;
      }

      this.addCronJob(
        reminder.cron_name,
        reminder.cron,
        reminder.groups.group_tlg_id,
        `${reminder.content} You are ${this.calculateAge(
          reminder.cron_name.split('=')[1],
        )} years old today!`,
      );
    });
  }

  calculateAge(birthdate: string): number {
    const birthDate = new Date(birthdate);
    const today = new Date();
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }

    return age;
  }
}
