/* eslint-disable prettier/prettier */
import { Command, Ctx, Help, Update } from 'nestjs-telegraf';

import { CronminderService } from './cronminder.service';
import { TelegrafContext } from '../telegraf/telegraf.interface';
import { getParams } from 'src/common/common.functions';

@Update()
export class CronminderUpdates {
  constructor(private readonly service: CronminderService) {}

  @Command('weekday')
  async scheduleWeekday(@Ctx() ctx: TelegrafContext) {
    const chatId = ctx.chat.id;
    const [name, text, hour] = getParams(ctx.message.text, '-', 3);

    if (!name || !text || !hour) {
      return `Please, give me a name, a message and an hour to send the message, separated by '-'`;
    }

    const isNewJobScheduled = this.service.scheduleWeekdayMessage(
      name,
      String(chatId),
      text,
    );

    if (isNewJobScheduled) {
      await ctx.reply(`Message scheduled to be sent at ${hour} on weekdays`);
    } else {
      await ctx.reply('Weekday message already scheduled');
    }
  }

  @Command('daily')
  async scheduleDailyMsg(@Ctx() ctx: TelegrafContext) {
    const chatId = ctx.chat.id;
    const [name, text, hour] = getParams(ctx.message.text, '-', 3);
    if (!name || !text || !hour) {
      return `Please, give me a name, a message and an hour to send the message, separated by '-'`;
    }

    const isNewJobScheduled = this.service.scheduleDaily(
      name,
      String(chatId),
      text,
    );
    if (isNewJobScheduled) {
      await ctx.reply(`Message scheduled to be sent at ${hour} every day`);
      return;
    }
    await ctx.reply('Daily message already scheduled');
  }

  @Command('monthly')
  async scheduleMonthlyMsg(@Ctx() ctx: TelegrafContext) {
    const chatId = ctx.chat.id;
    const [name, text, day, hour] = getParams(ctx.message.text, '-', 4);
    if (!name || !text || !day || !hour) {
      return `Please, give me a name, a message, a day and an hour to send the message, separated by '-'`;
    }

    const isNewJobScheduled = this.service.scheduleMonthly(
      name,
      String(chatId),
      text,
      day,
      hour,
    );

    if (isNewJobScheduled) {
      await ctx.reply(`Message scheduled to be sent at ${hour} on day ${day} of every month`);
      return;
    }
    await ctx.reply('Monthly message already scheduled');
  }

  @Command('weekly')
  async scheduleWeeklyMsg(@Ctx() ctx: TelegrafContext) {
    const chatId = ctx.chat.id;
    const [name, text, day, hour] = getParams(ctx.message.text, '-', 4);
    if (!name || !text || !day || !hour) {
      return `Please, give me a name, a message, a day and an hour to send the message, separated by '-'`;
    }

    const isNewJobScheduled = this.service.scheduleWeekly(
      name,
      String(chatId),
      text,
      day,
      hour,
    );

    if (isNewJobScheduled) {
      await ctx.reply(`Message scheduled to be sent at ${hour} on day ${day} of every week`);
      return;
    }
    await ctx.reply('Weekly message already scheduled');
  }

  @Command('days')
  async scheduleDays(@Ctx() ctx: TelegrafContext) {
    const chatId = ctx.chat.id;
    const [name, text, days, hour] = getParams(ctx.message.text, '-', 4);
    if (!name || !text || !days || !hour) {
      return `Please, give me a name, a message, days of the week (in numbers, separated by commas) and an hour to send the message, separated by '-'`;
    }

    const daysArray = days.split(',');

    const isNewJobScheduled = this.service.scheduleDays(
      name,
      String(chatId),
      text,
      daysArray,
      hour,
    );

    if (isNewJobScheduled) {
      await ctx.reply(`Message scheduled to be sent at ${hour} on days ${days}`);
      return;
    }
    await ctx.reply('Days message already scheduled');
  }

  @Command('yearly')
  async scheduleYearly(@Ctx() ctx: TelegrafContext) {
    const chatId = ctx.chat.id;
    const [name, text, day, month, hour] = getParams(ctx.message.text, '-', 5);
    if (!name || !text || !day || !month || !hour) {
      return `Please, give me a name, a message, a day, a month and an hour to send the message, separated by '-'`;
    }

    const isNewJobScheduled = this.service.scheduleYearly(
      name,
      String(chatId),
      text,
      day,
      month,
      hour,
    );

    if (isNewJobScheduled) {
      await ctx.reply(`Message scheduled to be sent at ${hour} on day ${day} of month ${month}`);
      return;
    }
    await ctx.reply('Yearly message already scheduled');
  }

  @Command('remind')
  async remind(@Ctx() ctx: TelegrafContext) {
    const chatId = ctx.chat.id;

    const params = ctx.message.text
      .replace(/\/[A-Za-z]+[^ ]* ?/, '')
      .split('_');
    if (!params.length || params.length < 3) {
      return;
    }

    const [format, name, message] = params;
    console.log(format, name, message);

    this.service.addCronJob(name, format, String(chatId), message);
    return 'Cron creado';
  }
  

  @Command('bday')
  async remindBday(@Ctx() ctx: TelegrafContext) {
    const chatId = ctx.chat.id;
    const dateUser = ctx.message.text.replace(/\/[A-Za-z]+[^ ]* ?/, '');
    console.log(dateUser);
    const user = ctx.message.from;
    const message = await this.service.addBdayReminder(
      user.first_name,
      user?.last_name ?? '',
      dateUser,
      String(chatId),
    );
    return message;
  }
}
