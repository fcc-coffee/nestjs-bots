import { Module } from '@nestjs/common';

import { KarmabotService } from './karmabot.service';
import { KarmabotUpdate } from './karmabot.update';

@Module({
  providers: [KarmabotService, KarmabotUpdate],
})
export class KarmabotModule {}
