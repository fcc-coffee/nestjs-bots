import { Injectable } from '@nestjs/common';

import { SupabaseService } from '../supabase/supabase.service';
import { UserDto } from './user.dto';
import { Supa } from '../common/common.interface';

@Injectable()
export class KarmabotService {
  private readonly supa: Supa;

  constructor(private supabaseService: SupabaseService) {
    this.supa = supabaseService.getClient();
  }

  async addKarma(groupId: string, userId: string, username?: string) {
    const group = await this.getGroup(groupId);
    const dbUser = await this.getUser(userId);

    const { data } = await this.supa
      .from('karmalation')
      .select('karma, id')
      .eq('user_id', dbUser.id)
      .eq('group_id', group.id)
      .limit(1)
      .single();

    const karma = (data?.karma ?? 0) + 1;

    const user: UserDto & { id: number } = {
      firstName: dbUser.first_name,
      userId: userId,
      id: dbUser.id,
      lastName: dbUser.last_name,
      username: username,
    };

    return await this.updateKarma(karma, data?.id ?? undefined, user, group);
  }

  async updateLastUse(userId: number, groupId: number) {
    const { id: userID } = await this.getUser(String(userId));
    const { id: groupID } = await this.getGroup(String(groupId));

    const { data, error: fetchError } = await this.supa
      .from('karmalation')
      .select('id')
      .eq('user_id', userID)
      .eq('group_id', groupID)
      .limit(1)
      .maybeSingle(); // maybeSingle returns null instead of error if no row is found.

    if (fetchError) {
      console.error(
        'Error fetching the karmalation entry:',
        fetchError.message,
      );
      return fetchError;
    }

    // Check if the entry exists.
    if (data) {
      // Entry exists, update it.
      const { error: updateError } = await this.supa
        .from('karmalation')
        .update({ updated_at: new Date().toISOString() })
        .eq('id', data.id); // Use the fetched id to ensure the correct row is updated.

      if (updateError) {
        console.error(
          'Error updating the karmalation entry:',
          updateError.message,
        );
        return updateError;
      }
    } else {
      // Entry does not exist, insert a new one.
      const { error: insertError } = await this.supa
        .from('karmalation')
        .insert({
          updated_at: new Date().toISOString(),
          user_id: userID,
          group_id: groupID,
          // Set other necessary fields, such as karma, etc.
        });

      if (insertError) {
        console.error(
          'Error inserting a new karmalation entry:',
          insertError.message,
        );
        return insertError;
      }
    }

    // Return null to indicate success.
    return null;
  }

  async updateKarma(
    karma: number,
    id: number | null, // Allow null for new entries
    user: UserDto & { id: number },
    group: {
      group_tlg_id: string;
      id: number;
      name: string;
    },
  ) {
    // Perform upsert
    const payload = {
      karma,
      user_id: user.id,
      username:
        user?.username ?? `${user.firstName} ${user?.lastName ?? ''}`.trim(),
      group_id: group.id,
    };
    //@ts-ignore
    if (id !== null) payload.id = id; // Include id only for updates

    const { error } = await this.supa.from('karmalation').upsert(payload);

    if (error) return { error, karma: 0 };

    return { error: null, karma };
  }

  async takeKarma(groupId: string, userId: string, username?: string) {
    const group = await this.getGroup(groupId);
    const { id, first_name, last_name } = await this.getUser(userId);

    const { data } = await this.supa
      .from('karmalation')
      .select('karma, id')
      .eq('user_id', id)
      .eq('group_id', group.id)
      .limit(1)
      .single();

    const karma = (data?.karma ?? 0) - 1;

    const user: UserDto & { id: number } = {
      firstName: first_name,
      userId: userId,
      id: id,
      lastName: last_name,
      username: username,
    };

    return await this.updateKarma(karma, data.id, user, group);
  }

  async getUser(userId: string) {
    const { data } = await this.supa
      .from('users')
      .select('*')
      .eq('user_id', userId)
      .limit(1)
      .single();

    return data;
  }

  async getGroup(groupId: string) {
    const { data } = await this.supa
      .from('groups')
      .select('*')
      .eq('group_tlg_id', groupId)
      .limit(1)
      .maybeSingle();

    return data;
  }

  async createGroup(groupId: string, groupName: string) {
    const res = await this.getGroup(groupId);

    if (res) return null;

    const { error } = await this.supa
      .from('groups')
      .upsert({ name: groupName, group_tlg_id: groupId });
    if (error) return error;

    return null;
  }

  async upsertUser(groupName: string, groupId: string, user: UserDto) {
    let count: number;
    ({ count } = await this.supa
      .from('groups')
      .select('*')
      .eq('group_tlg_id', groupId));

    const error = !count ? await this.createGroup(groupId, groupName) : null;
    if (error) return error;

    ({ count } = await this.supa
      .from('users')
      .select('*')
      .eq('user_id', user.userId));

    if (!count) {
      const { error } = await this.supa.from('users').upsert({
        first_name: user.firstName,
        user_id: user.userId,
        last_name: user?.lastName ?? '',
      });
      if (error) return error;
    }

    return null;
  }

  async userExists(userId: string): Promise<boolean> {
    const { data } = await this.supa
      .from('users')
      .select('id')
      .eq('user_id', userId)
      .limit(1)
      .single();

    return !!data;
  }

  async groupExists(groupId: string): Promise<boolean> {
    const { data } = await this.supa
      .from('groups')
      .select('id')
      .eq('group_tlg_id', groupId)
      .limit(1)
      .single();

    return !!data;
  }

  async canUpdateKarma(userId: string, groupId: string): Promise<boolean> {
    const group = await this.getGroup(groupId);
    if (!group) return true;

    const { id: groupID } = group;

    const user = await this.getUser(userId);
    if (!user) return true;

    const { id: userID } = user;

    const { data, error } = await this.supa
      .from('karmalation')
      .select('updated_at')
      .eq('user_id', userID)
      .eq('group_id', groupID)
      .limit(1)
      .maybeSingle();

    if (error) {
      console.log(error);
      throw new Error('Error fetching last update time');
    }

    // If no record found or updated_at is not set, allow update
    if (!data || !data.updated_at) return true;

    const lastUpdateDate = new Date(data.updated_at);
    const currentDate = new Date();
    const differenceInMinutes =
      (currentDate.getTime() - lastUpdateDate.getTime()) / 60000;

    return differenceInMinutes >= 1;
  }

  async getKarma(userId: string, groupId: string): Promise<number> {
    const group = await this.getGroup(groupId);
    const { id } = await this.getUser(userId);

    const { data, error } = await this.supa
      .from('karmalation')
      .select('karma')
      .eq('user_id', id)
      .eq('group_id', group.id)
      .limit(1)
      .single();
    console.log(data);
    console.log(error);
    return data?.karma ?? 0;
  }

  async getTopKarma(groupId: string) {
    const group = await this.getGroup(groupId);

    const { data, error } = await this.supa
      .from('karmalation')
      .select('karma, username')
      .eq('group_id', group.id)
      .order('karma', { ascending: false })
      .limit(10);

    if (error) {
      console.error('Error fetching top karma:', error.message);
      return [];
    }

    return data;
  }

  async getLowKarma(groupId: string) {
    const group = await this.getGroup(groupId);

    const { data, error } = await this.supa
      .from('karmalation')
      .select('karma, username')
      .eq('group_id', group.id)
      .order('karma', { ascending: true })
      .limit(10);

    if (error) {
      console.error('Error fetching low karma:', error.message);
      return [];
    }

    return data;
  }
}
