import { Command, Ctx, Hears, On, Update } from 'nestjs-telegraf';

import { KarmabotService } from './karmabot.service';
import { TelegrafContext } from '../telegraf/telegraf.interface';
import { UserDto } from './user.dto';

@Update()
export class KarmabotUpdate {
  constructor(private karma: KarmabotService) {}

  @Command('mykarma')
  async myKarma(ctx: TelegrafContext) {
    console.log('aaa');
    const user = ctx.message.from;
    const chat = ctx.message.chat;

    const userExists = await this.karma.userExists(String(user.id));
    if (!userExists) {
      await ctx.reply('You have no karma points');
    }
    const karma = await this.karma.getKarma(String(user.id), String(chat.id));

    await ctx.reply(`${user.first_name} has ${karma} karma points`);
  }

  @Command('top')
  async topKarma(ctx: TelegrafContext) {
    const chat = ctx.message.chat;
    const top = await this.karma.getTopKarma(String(chat.id));

    let message = '';
    top.forEach((user, index) => {
      message += `${index + 1}. ${user.username} - ${user.karma}\n`;
    });

    await ctx.reply(message);
  }

  @Command('low')
  async lowKarma(ctx: TelegrafContext) {
    const chat = ctx.message.chat;
    const low = await this.karma.getLowKarma(String(chat.id));

    let message = '';
    low.forEach((user, index) => {
      message += `${index + 1}. ${user.username} - ${user.karma}\n`;
    });

    await ctx.reply(message);
  }

  async insert(
    user: {
      first_name: string;
      id: number;
      last_name?: string;
      username?: string;
    },
    chat: { title: string; id: number },
  ) {
    const name = chat.title;
    const chatId = String(chat.id);

    const from = new UserDto();
    from.firstName = user.first_name;
    from.userId = String(user.id);
    from.username =
      user.username ?? `${user.first_name} ${user.last_name ?? ''}`.trim();
    from.lastName = user.last_name ?? '';

    return await this.karma.upsertUser(name, chatId, from);
  }

  @On('message')
  async checkMessage(@Ctx() ctx: TelegrafContext) {
    const message = ctx.message.text;
    const chat = ctx.message.chat;
    const karmaTransmit = ctx.message.from;
    const karmaReceiver = ctx.message.reply_to_message?.from ?? null;

    const containsPlus = /^\s*\+1\s*$/.test(message);
    const containsMinus = /^\s*-1\s*$/.test(message);

    if (!karmaReceiver && (containsMinus || containsPlus)) {
      await ctx.reply('You have to reply to a message');
      return;
    }

    if (karmaReceiver?.id === karmaTransmit?.id && containsPlus) {
      await ctx.reply('You cannot give karma to yourself');
      return;
    }

    if (karmaReceiver?.id === karmaTransmit?.id && containsMinus) {
      await ctx.reply('You cannot take karma from yourself');
      return;
    }

    const username =
      karmaReceiver?.username ??
      `${karmaReceiver?.first_name} ${karmaReceiver?.last_name ?? ''}`.trim();

    const receiverExists = await this.karma.userExists(
      String(karmaReceiver?.id),
    );
    const transmitExists = await this.karma.userExists(
      String(karmaTransmit?.id),
    );

    if (chat.type === 'private') {
      await ctx.reply('Use me only in groups');
      return;
    }

    const groupExists = await this.karma.groupExists(String(chat.id));

    if (!receiverExists) {
      const error = await this.insert(karmaReceiver, chat);
      if (error) {
        console.log(error);
        await ctx.reply(error.message);
      }
    }

    if (!transmitExists) {
      const error = await this.insert(karmaTransmit, chat);
      if (error) {
        console.log(error);
        await ctx.reply(error.message);
      }
    }

    if (!groupExists) {
      const error = await this.karma.createGroup(String(chat.id), chat.title);
      if (error) return error.message;
    }

    const canUpdate = await this.karma.canUpdateKarma(
      String(karmaTransmit.id),
      String(chat.id),
    );

    if ((!canUpdate && containsPlus) || (!canUpdate && containsMinus)) {
      await ctx.reply(
        'You have to wait 1 minute to add or substract karma again',
      );
      return;
    } else if (containsMinus) {
      const { error, karma } = await this.karma.takeKarma(
        String(chat.id),
        String(karmaReceiver.id),
        username,
      );
      if (error) {
        await ctx.reply(error.message);
      }

      const updateError = await this.karma.updateLastUse(
        karmaTransmit.id,
        chat.id,
      );
      console.log(updateError);
      if (updateError) {
        await ctx.reply(updateError.message);
      }

      await ctx.reply(
        `${karmaReceiver.first_name} has now ${karma} karma points`,
      );
    } else if (containsPlus) {
      const { error, karma } = await this.karma.addKarma(
        String(chat.id),
        String(karmaReceiver.id),
        username,
      );
      if (error) {
        await ctx.reply(error.message);
      }

      const updateError = await this.karma.updateLastUse(
        karmaTransmit.id,
        chat.id,
      );
      console.log(updateError);
      if (updateError) {
        await ctx.reply(updateError.message);
      }

      await ctx.reply(
        `${karmaReceiver.first_name} has now ${karma} karma points`,
      );
    }
  }
}
