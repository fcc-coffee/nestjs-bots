import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule); // Crea NestApplication en lugar de ApplicationContext
  const port = process.env.PORT || 8080; // Utiliza un puerto del entorno o un valor por defecto
  await app.listen(port, '0.0.0.0'); // Escucha en todas las interfaces de red
  app.get(ConfigService);
}

bootstrap();
