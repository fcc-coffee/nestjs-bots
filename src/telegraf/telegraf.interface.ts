import { Context } from 'telegraf';
import { Message, Update } from '@telegraf/types';

export interface TelegrafContext extends Context {
  message: Message.CommonMessage &
    Message.TextMessage &
    Update.New &
    Update.NonChannel &
    Message;
}
